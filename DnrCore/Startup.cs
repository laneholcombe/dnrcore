using System;
using System.IO;
using System.Reflection;
using log4net.Config;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using CustomMiddleware;
using CalculatorNameSpace;
using System.ServiceModel;

namespace DnrCore
{
  public class Startup
  {
    static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(Startup));
    public Startup(IConfiguration configuration)
    {
      Log.Debug("Startup");
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
      Log.Debug("ConfigureServices");
      services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

      Log.Debug("Starting Calculator Singleton");
      services.AddSingleton<CalculatorService>();

      // In production, the React files will be served from this directory
      services.AddSpaStaticFiles(configuration =>
      {
        configuration.RootPath = "ClientApp/build";
      });
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IHostingEnvironment env)
    {
      Log.DebugFormat("Configure [ app: {0}, env: {1} ]", app, env);
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }
      else
      {
        app.UseExceptionHandler("/Error");
        app.UseHsts();
      }
      Log.Debug("Binding to CalculatorService");
      IApplicationBuilder builder = app.UseSOAPEndpoint<CalculatorService>("/CalculatorService.svc", new BasicHttpBinding());
      if (builder == null) {
        Log.Debug("Could not instantiate SOAP Endpoint middleware");
      } else {
        Log.DebugFormat("SOAP Endpoint middleware is {0}", builder);
      }
      app.UseHttpsRedirection();
      app.UseStaticFiles();
      app.UseSpaStaticFiles();

      app.UseMvc(routes =>
      {
        routes.MapRoute(
          name: "default",
          template: "{controller}/{action=Index}/{id?}");
      });

      app.UseSpa(spa =>
      {
        spa.Options.SourcePath = "ClientApp";

        if (env.IsDevelopment())
        {
          Log.Debug("Starting React Development Server.");
          spa.UseReactDevelopmentServer(npmScript: "start");
        }
      });

    }
  }
}
