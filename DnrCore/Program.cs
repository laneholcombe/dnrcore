using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using log4net.Config;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace DnrCore
{
    public class Program
    {
        static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(Startup));
        public static void Main(string[] args)
        {
            var dnrcore = Assembly.GetEntryAssembly();
            log4net.Repository.ILoggerRepository logRepository = log4net.LogManager.GetRepository(dnrcore);
            XmlConfigurator.Configure(logRepository, new FileInfo("log4net.xml"));
            
            Log.Debug("============================================================");
            Log.Debug("============================================================");
            Log.DebugFormat("Startup.  DnrCore is {0}", dnrcore);

            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
