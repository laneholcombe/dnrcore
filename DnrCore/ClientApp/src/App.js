import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { FetchData } from './components/FetchData';
import { Counter } from './components/Counter';
import { D3 } from "./components/D3";

export default class App extends Component {
  displayName = App.name

  render() {
    return (
      <Layout>
        <Route exact path='/' component={Home} />
        <Route path='/video' component={Counter} />
        <Route path='/fetchdata' component={FetchData} />
        <Route path='/d3' component={D3} />
      </Layout>
    );
  }
}
