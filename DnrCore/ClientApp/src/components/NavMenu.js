﻿import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Glyphicon, Nav, Navbar, NavItem, Panel } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import './NavMenu.css';

export class NavMenu extends Component {
  displayName = NavMenu.name

  render() {
    return (
        <Panel>
            <Panel>
                Inner Panel
            </Panel>
          <Panel>
          <Navbar inverse fixedTop fluid collapseOnSelect>
            <Navbar.Header>
              <Navbar.Brand>
                <Link to={'/'}>.NET + React</Link>
              </Navbar.Brand>
              <Navbar.Toggle />
            </Navbar.Header>
            <Navbar.Collapse>
              <Nav>
                <LinkContainer to={'/d3'} exact>
                  <NavItem>
                    <Glyphicon glyph='stats' /> D3
                  </NavItem>
                </LinkContainer>
                <LinkContainer to={'/'} exact>
                  <NavItem>
                    <Glyphicon glyph='home' /> Home
                  </NavItem>
                </LinkContainer>
                <LinkContainer to={'/video'}>
                  <NavItem>
                    <Glyphicon glyph='education' /> Our Backyard
                  </NavItem>
                </LinkContainer>
                <LinkContainer to={'/fetchdata'}>
                  <NavItem>
                    <Glyphicon glyph='th-list' /> Fetch data
                  </NavItem>
                </LinkContainer>
              </Nav>
            </Navbar.Collapse>
          </Navbar>
          </Panel>
          <Panel>
          Footer
          </Panel>
      </Panel>
    );
  }
}
