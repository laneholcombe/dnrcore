import React, { Component } from 'react';
import XMLParser from "react-xml-parser";
import _ from "lodash";
import RGL, { WidthProvider } from "react-grid-layout";

const ReactGridLayout = WidthProvider(RGL);

export class Home extends Component {
  static defaultProps = {
    className: "layout",
    isDraggable: false,
    isResizable: false,
    items: 5,
    rowHeight: 20,
    
    onLayoutChange: function() {},
    cols: 4
  };
  displayName = Home.name
  constructor(props) {
    super(props);
    console.log(`constructor({ props: ${JSON.stringify(props)}) }`);
    this.calculate = this.calculate.bind(this);
    this.generateSoap = this.generateSoap.bind(this);
    this.onChangeOperation = this.onChangeOperation.bind(this);
    this.onValueChange = this.onValueChange.bind(this);
    this.getResult = this.getResult.bind(this);
    this.generateDOM = this.generateDOM.bind(this);
    this.state = {};
  }
  
  generateDOM() {
    return _.map(_.range(this.props.items), (i) => {
      let content;
      switch (i) {
        case 0:
          content = <div>
            <h1>Calculator</h1>
            <p>Demonstrates SOAP client/server</p>
            </div>;
          break;
        case 1:
          content = <div className="static"><p>First argument:&nbsp;<input name="value1" type="number" value={this.state.value1} onChange={this.onValueChange} maxLength="4" /></p></div>;
          break;
        case 2:
          content = <div className="static"><p>Action:&nbsp;
                    <select name="operation" onChange={this.onChangeOperation} defaultValue={ this.state.SOAPAction }>
                      <option value="Add">Add</option>
                      <option value="Subtract">Subtract</option>
                      <option value="Multiply">Multiply</option>
                      <option value="Divide">Divide</option>
                    </select>
                    </p>
                    </div>;
          break;
        case 3:
          content = <div className="static">
                    <p>Second argument: <input name="value2" type="number" value={this.state.value2} onChange={this.onValueChange}/></p>
                    </div>;
          break;
        case 4:
          content = <div className="static">
                    Result:  { this.state.result ? this.state.result : ""}
                    <button onClick={this.calculate}>Solve</button>
                    </div>;
          break;
        default:
          content = <div className="static">Unexpected { i } </div>;
          break;
      }
      return (
        <div key={i}>
          <span className="text">{ content }</span>
        </div>
      );
    });
  }

  generateLayout() {
    console.log(`generateLayout() props: ${JSON.stringify(this.props)}`);
    const p = this.props;
    return _.map(new Array(p.items), (item, i) => {
      const y = _.result(p, "y") || Math.ceil(Math.random() * 4) + 1;
      return {
        x: (i * 2) % 12,
        y: Math.floor(i / 6) * y,
        w: 2,
        h: y,
        i: i.toString()
      };
    });
  }

  onLayoutChange(layout) {
    this.props.onLayoutChange(layout);
  }

  componentWillMount() {
    console.log(`componentWillMount()`);
    let state = this.state;
    state.value1 = 19;
    state.value2 = 12;
    state.SOAPAction = "Divide";
    state.layout = this.generateLayout();
    console.log(`state is ${JSON.stringify(state)}`);
    this.setState(state);
  }
  onValueChange = (e) => {
    let state = this.state;
    state[e.target.name] = e.target.value;
    this.setState(state);    
  }
  generateSoap() {
    return '<?xml version="1.0" encoding="utf-8"?>' +
      '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' +
        '<s:Body>' +
          `<${this.state.SOAPAction} xmlns="http://joeandlane.com/">` +
            `<x>${ this.state.value1 }</x>` +
            `<y>${ this.state.value2 }</y>` +
          `</${this.state.SOAPAction}>` + 
      '</s:Body>' + 
    '</s:Envelope>';
  }
  getResult(xmlString) {
    let xml = new XMLParser().parseFromString(xmlString);
    let result = xml.getElementsByTagName(this.state.SOAPAction + 'Result')[0];
    return result.value;
  }
  calculate = (e) => {
  // Method: POST
  // content-type: text/xml
  // accept: text/xml
  // SOAPAction = this.state.SOAPAction
    let soapRequest = this.generateSoap();
    let options = {
      method: "POST",
      body: soapRequest,
      headers: {
        "Content-type": "text/xml",
        "Accept": "text/xml",
        "SOAPAction": `http://joeandlane.com/ICalculatorService/${this.state.SOAPAction}`
      }
    };
    console.log(`sending request ${soapRequest} with headers ${JSON.stringify(options.headers)}`);
    fetch('/CalculatorService.svc', options)
      .then(response => {
        console.log(`response is ${JSON.stringify(response)}`);
        return response.text();
      })
      .then(data => {
        console.log(`data is ${data}`);
        let state = this.state;
        state.result = this.getResult(data);
        this.setState(state);
      });
  }
  onChangeOperation  = (e) => {
    let index = e.nativeEvent.target.selectedIndex;
    console.log(`index: ${index}, text = ${e.nativeEvent.target[index].text}`);
    let state = this.state;
    state.SOAPAction = e.nativeEvent.target[index].text;
    this.setState(state);    
  }
  render() {
    console.log(`layouts = ${JSON.stringify(this.state.layout)}`);
    return (
      <div>
        <ReactGridLayout
          layout={this.state.layout}
          onLayoutChange={this.onLayoutChange}
          {...this.props}
        >
          {this.generateDOM()}
        </ReactGridLayout>
      </div>
    );
  }
}
