import React, { Component } from 'react';
import { Col, MenuItem, Nav, NavDropdown, NavItem, Row, Tab, } from 'react-bootstrap';

import { AreaChart } from "react-easy-chart";
import { BarChart } from "react-easy-chart"; 
import { LineChart } from "react-easy-chart";
import { PieChart } from "react-easy-chart"; 
import { ScatterplotChart } from "react-easy-chart";

const width = 1200;
const height = 1000;
let nextId = 1;
export class D3 extends Component {
  displayName = D3.name;
  getId(data) {
    return `getId(${data}):${nextId++}`;
  }
  constructor(props) {
    super(props);
    this.mouseOverHandler=this.mouseOverHandler.bind(this);
    this.mouseOutHandler=this.mouseOutHandler.bind(this);
    this.mouseMoveHandler=this.mouseMoveHandler.bind(this);
    this.buttonClick = this.buttonClick.bind(this);
    this.getPieData = this.getPieData.bind(this);
    this.getLineData = this.getLineData.bind(this);
    this.getScatterData = this.getScatterData.bind(this);
    
    this.state = {
      width: width,
      height: height,
      data: [],
      linedata: [],
      piedata: [],
      scatterdata: [],
      loading: true,
      tooltip: { show: false}
    };
    this.buttonClick();
    this.getPieData();
    this.getLineData();
    this.getScatterData();
  }
  mouseOverHandler(d, e) {
    let state = this.state;
    state.tooltip = {
      show: true,
      top: `${e.screenY - 10}px`,
      left: `${e.screenX + 10}px`,
      tip: `${d.x} = ${d.y}`,
      y: d.y,
      x: d.x
    };
    this.setState(state);
  }

  mouseMoveHandler(e) {
    if (this.state.tooltip.show) {
      let state = this.state;
      state.tooltip.top = `${e.y - 10}px`;
      state.tooltip.left = `${e.x + 10}px`;
      this.setState(state);
    }
  }

  mouseOutHandler() {
    let state = this.state;
    state.tooltip.show = false;
    this.setState(state);
  }
  buttonClick() {
    console.log(`buttonClick()`);
    fetch("api/SampleData/GetChartData")
      .then(response => response.json())
      .then(data => {
        console.log(`chartData ${JSON.stringify(data)}`);
        let state = this.state;
        state.data = data;
        state.loading = false;
        this.setState(state);
      });
  }
  getPieData() {
    console.log(`getPieData()`);
    fetch(`api/SampleData/getPieData`)
    .then(response => response.json())
    .then(data => {
      console.log(`pieData ${JSON.stringify(data)}`);
      let state = this.state;
      state.piedata = data;
      state.loading = false;
      this.setState(state);
    });
  }
  getLineData() {
    console.log(`getLineData()`);
    fetch(`api/SampleData/getLineData`)
    .then(response => response.json())
    .then(data => {
      console.log(`lineData ${JSON.stringify(data)}`);
      let state = this.state;
      state.linedata = data;
      state.loading = false;
      this.setState(state);      
    })
  }
  getScatterData() {
    console.log(`getScatterData()`);
    fetch(`api/SampleData/getScatterData`)
    .then(response => response.json())
    .then(data => {
      console.log(`scatterData ${JSON.stringify(data)}`);
      let state = this.state;
      state.scatterdata = data;
      state.loading = false;
      this.setState(state);      
    })
  }
  
  render() {
    return (
      <div>
        <h1>D3</h1>
        <p>This is a simple example of a D3 React component.</p>
        <Tab.Container defaultActiveKey="first" generateChildId={this.getId}>
          <Row className="clearfix">
            <Col sm={12}>
              <Nav bsStyle="tabs">
                <NavItem eventKey="first">Bar Chart</NavItem>
                <NavItem eventKey="second">Pie Chart</NavItem>
                <NavDropdown eventKey="3" title="Dropdown" >
                  <MenuItem eventKey="3.1">Line Chart</MenuItem>
                  <MenuItem eventKey="3.2">Area Chart</MenuItem>
                  <MenuItem eventKey="3.3">Scatter Plot</MenuItem>
                  <MenuItem divider />
                  <MenuItem eventKey="3.4">Separated link</MenuItem>
                </NavDropdown>
              </Nav>
            </Col>
            <Col sm={12}>
              <Tab.Content animation>
                <Tab.Pane eventKey="first">
                <BarChart 
                  axes
                  axisLabels={{x: 'https://rma-consulting.github.io/react-easy-chart/bar-chart/index.html', y: 'None' }}
                  colorBars
                  height={350}
                  width={550}
                  data={ this.state.data }
                  margin={{top: 10, right: 0, bottom: 30, left: 50}}
                  yAxisOri entRight
                  mouseOverHandler={this.mouseOverHandler}
                  mouseOutHandler={this.mouseOutHandler}
                  mouseMoveHandler={this.mouseMoveHandler}
                />
                {this.state.tooltip.show &&
                  <div style={{ position: "absolute", left: this.state.tooltip.left }}>{this.state.tooltip.tip}</div>
                }
                <label><a target='_new' href='https://rma-consulting.github.io/react-easy-chart/bar-chart/index.html'>More information</a></label>
                <button onClick={this.buttonClick}>Start</button>
                </Tab.Pane>
                <Tab.Pane eventKey="second">
                  <PieChart
                    labels
                    size={600}
                    data={ this.state.piedata }
                  />                
                <button onClick={this.getPieData}>Refresh</button>
                <label><a target='_new' href='https://rma-consulting.github.io/react-easy-chart/pie-chart/index.html'>More information</a></label>
                </Tab.Pane>
                <Tab.Pane eventKey="3.1">
                  This should be a line chart ... hmm ...
                  <LineChart 
                    axes
                    axisLabels={{x: 'X Axis', y: 'Y Axis'}}
                    interpolate={'cardinal'}
                    yType={'text'}
                    xType={'text'}
                    width={500}
                    height={500}
                    grid
                    data={ this.state.linedata }
                  />
                  <label><a target='_new' href='https://rma-consulting.github.io/react-easy-chart/line-chart/index.html'>More information</a></label>
                <button onClick={this.getLineData}>Refresh</button>
                </Tab.Pane>
                <Tab.Pane eventKey="3.2">
                    <AreaChart
                      axes
                      axisLabels={{x: 'X Axis', y: 'Y Axis'}}
                      interpolate={'cardinal'}
                      yType={'text'}
                      xType={'text'}
                      width={500}
                      height={500}
                      grid
                      data={ this.state.linedata }
                    />
                    <button onClick={this.getLineData}>Refresh</button>
                    <label><a target='_new' href='https://rma-consulting.github.io/react-easy-chart/area-chart/index.html'>More information</a></label>
                  </Tab.Pane>
                <Tab.Pane eventKey="3.3">
                  A Scatter Plot Demo
                  <ScatterplotChart 
                    data={ this.state.scatterdata } />
                    <button onClick={this.getScatterData}>Refresh</button>
                    <label><a target='_new' href='https://rma-consulting.github.io/react-easy-chart/scatterplot-chart/index.html'>More information</a></label>
                </Tab.Pane>
                <Tab.Pane eventKey="3.4">Tab 3.4 content</Tab.Pane>
              </Tab.Content>
            </Col>
          </Row>
        </Tab.Container>
      </div>
    );
  }
}
