import React from "react";
import { Panel } from "react-bootstrap";

import { BaseComponent } from "./base/BaseComponent";

export class Resume extends BaseComponent {
  displayName = Resume.name;

  render() {
    window.setTimeout(() => {
      window.open("http://joeandlane.com/www/", "_blank");
    });
    return (
      <Panel height={(this.state.height * 9) / 7}>
        <Panel.Heading width={this.state.width * 1.2}>
          Just for fun
        </Panel.Heading>
        <Panel.Body className="clear">
          <iframe
            title="Lane Holcombe"
            src="http://joeandlane.com/www/"
            height={(this.state.height * 8) / 7}
            width={this.state.width}
          />
        </Panel.Body>
        <Panel.Footer>
          Let&quot;s do it in <a href="https://cordova.apache.org/">Cordova!</a>
        </Panel.Footer>
      </Panel>
    );
  }
}
