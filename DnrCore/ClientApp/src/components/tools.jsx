import * as _ from "underscore";

                                                                                                                                                                                                                                                                                                                                                                                                                                    let links = [];
let nodes = [];

const randomData = (nodes, width, height, limit) => {
    let bounds = { high: limit, low: 5};
    let size = { high: 10, low: 4 };
    let oldNodes = nodes;
    // generate some data randomly
    nodes = _.chain(_.range(_.random(bounds.low, bounds.high)))
    .map(() => {
        let node = {};
        node.key = _.random(bounds.low, bounds.high);
        node.size = _.random(size.low, size.high);

        return node;
    }).uniq((node) => {
        return node.key;
    }).value();

    if (oldNodes) {
        let add = _.initial(oldNodes, _.random(0, oldNodes.length));
        add = _.rest(add, _.random(0, add.length));

        nodes = _.chain(nodes)
        .union(add).uniq((node) => {
            return node.key;
        }).value();
    }

    links = _.chain(_.range(_.random(15, 85)))
        .map(() => {
            let link = {};
            link.source = _.random(0, nodes.length - 1);
            link.target = _.random(0, nodes.length - 1);
            link.key = link.source + ',' + link.target;
            link.size = _.random(1, 3);
            return link;
        }).uniq((link) => link.key)
        .value();

    maintainNodePositions(oldNodes, nodes, width, height);
    return {nodes, links};
};

const maintainNodePositions = (oldNodes, nodes, width, height) => {
    let kv = {};
    _.each(oldNodes, (d) => {
      kv[d.key] = d;
    });
    _.each(nodes, (d) => {
      if (kv[d.key]) {
        // if the node already exists, maintain current position
        d.x = kv[d.key].x;
        d.y = kv[d.key].y;
      } else {
        // else assign it a random position near the center
        d.x = width / 2 + _.random(-150, 150);
        d.y = height / 2 + _.random(-25, 25);
      }
    });
  };
let data = {
    links,
    nodes
};

export {
    randomData,
    data
};