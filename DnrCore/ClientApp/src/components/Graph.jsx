﻿// from: http://bl.ocks.org/sxywu/fcef0e6dac231ef2e54b
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import * as d3 from "d3";

import { data, randomData } from "./tools";

export default class Graph extends Component {
    constructor() {
        super();
        this.enterNode = this.enterNode.bind(this);
        this.updateNode = this.updateNode.bind(this);
        this.enterLink = this.enterLink.bind(this);
        this.updateGraph = this.updateGraph.bind(this);
        this.show = this.show.bind(this);
        this.localdrag = this.localdrag.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);
        this.getForce = this.getForce.bind(this);
        this.dragstarted = this.dragstarted.bind(this);
        this.dragged = this.dragged.bind(this);
        this.dragended = this.dragended.bind(this);
        this.times = 0;
    }

    dragstarted(d) {
      console.log(`dragstarted`);
      if (!d3.event.active) {
        this.getForce().alphaTarget(0.3).restart();
      }
      d.fx = d.x;
      d.fy = d.y;
    }
    dragged(d) {
      d.fx = d3.event.x;
      d.fy = d3.event.y;
    }
    dragended(d) {
      if (!d3.event.active) {
        this.getForce().alphaTarget(0);
      }
      d.fx = null;
      d.fy = null;
    }   
    getForce() {
      // This is actually the simulation, I think
      this.force =  d3.forceSimulation() //this.props.nodes);
      .force("link", d3.forceLink().id((d) =>  d.index ))
      .force("collide",d3.forceCollide( (d) => { return d.r + 8 }).iterations(16) )
      .force("charge", d3.forceManyBody())
      .force("center", d3.forceCenter(this.props.width / 2, this.props.height / 2))
      .force("y", d3.forceY(0))
      .force("x", d3.forceX(0));
//      console.log(`getForce() ${this.force}`);
      this.force.alphaTarget = 10;
//      console.log(`strength = ${this.props.strength}`);
      this.force.force("link").strength(+(100/+this.props.strength));
      return this.force;

      // Consider these original values:
//        .charge(-300)
//        .linkDistance(50)
//        .size([this.props.width, this.props.height]);
    }
    show = (item) => {
        let ar = [];
//        console.log(`showing ${item}`);
        Object.keys(item).forEach((key) => {
            ar.push(`${key} = ${item[key]}`);
        });
        return ar;
    }
    localdrag(whatever) {
      let retval = null;
//      console.log(`localdrag ${whatever}`);
      if (this.getForce().drag) {
        console.log(`drag`);
        retval = this.getForce().drag.apply(this);
        if (retval) {
          console.warn(`localdrag retval is ${retval}`);
          return retval;
        }
      } else {
        console.log(`no this.force.drag`);
      }
    }
    enterNode = (selection) => {
//        console.log(`enterNode: ${selection}`);
        selection.classed('node', true);
//        console.log(`selection.classed(node, true)`);
        selection.append('circle')
          .attr("r", (d) => {
            return d.size;
          })
          .call(d3.drag)
            .on("start", this.dragstarted)
            .on("drag", this.dragged)
            .on("end", this.dragended);
            //this.d3Graph      
        selection.append('text')
          .attr("x", (d) => d.size + 5)
          .attr("dy", ".35em")
          .text((d) => d.key);
      };
      
      updateNode = (selection) => {
//        console.log(`updateNode ${selection}`);
        selection.attr("transform", (d) => "translate(" + d.x + "," + d.y + ")");
      };
      
      enterLink = (selection) => {
//        console.log(`enterLink ${selection}`);
        selection.classed('link', true)
          .attr("stroke-width", (d) => {
//            console.log(`enterLink: ${this.props.strength}`);
            return d.size;
          });
      };
      
      updateLink = (selection) => {
//        console.log(`updateLink ${JSON.stringify(selection)}`);
        selection.attr("x1", (d) => d.source.x)
          .attr("y1", (d) => d.source.y)
          .attr("x2", (d) => d.target.x)
          .attr("y2", (d) => d.target.y);
      };
      updateGraph = (selection) => {
//        console.log(`updateGraph ${selection}`);
        selection.selectAll('.node')
          .call(this.updateNode);
        selection.selectAll('.link')
          .call(this.updateLink);
      };


    componentWillMount() {
      console.log(`componentWillMount: ${JSON.stringify(this.props)}`);
//      this.getForce().initialize(this.props.nodes);
    }
    componentDidMount() {
//      console.log(`componentDidMount: ${JSON.stringify(this.props)}`);
      this.d3Graph = d3.select(ReactDOM.findDOMNode(this.refs.graph));
//        this.force.each('tick', () => {
//            console.log(`force:onTick`);
            // after force calculation starts, call updateGraph
            // which uses d3 to manipulate the attributes,
            // and React doesn't have to go through lifecycle on each tick
//            this.d3Graph.call(this.updateGraph);
//        });
    }
    ticked() {
      let line = this.d3Graph
      .selectAll("line")
      .attr("stroke", "black")
      .attr("x1", (d) => { return d.source.x; })
      .attr("y1", (d) => { return d.source.y; })
      .attr("x2", (d) => { return d.target.x; })
      .attr("y2", (d) => { return d.target.y; });

//  node
//      .attr("cx", function(d) { return d.x; })
//      .attr("cy", function(d) { return d.y; });
      //        this.force.each('tick', () => {
//            console.log(`force:onTick`);
            // after force calculation starts, call updateGraph
            // which uses d3 to manipulate the attributes,
            // and React doesn't have to go through lifecycle on each tick
//            this.d3Graph.call(this.updateGraph);
//        });

    }
    shouldComponentUpdate(nextProps) {
//        console.log(`shouldComponentUpdate: ${JSON.stringify(nextProps)}`);
        this.d3Graph = d3.select(ReactDOM.findDOMNode(this.refs.graph));
//        console.log(`d3Graph is ${this.d3Graph}`);
        const d3Nodes = this.d3Graph.selectAll('.node')
          .data(nextProps.nodes, (node) => node.key);
          // todo
//          console.log(`there are ${d3Nodes.size()} nodes`);
        
        d3Nodes.enter().append('g').call(this.enterNode);
        d3Nodes.exit().remove();
        d3Nodes.call(this.updateNode);
    
        const d3Links = this.d3Graph.selectAll('.link')
          .data(nextProps.links, (link) => {
//            console.log(`link is ${JSON.stringify(link)}`);
            return link.key;
          });
        d3Links.enter().insert('line', '.node').call(this.enterLink);
        d3Links.exit().remove();
        d3Links.call(this.updateLink);
    
        // we should actually clone the nodes and links
        // since we're not supposed to directly mutate
        // props passed in from parent, and d3's force function
        // mutates the nodes and links array directly
        // we're bypassing that here for sake of brevity in example
        this.getForce().nodes(nextProps.nodes)
          .on("tick", () => {
//            console.log(`tick`) ;
            this.d3Graph.call(this.updateGraph)
          });

        //.links(nextProps.links);
        //this.force.start();
          
        return true;
    } 
    render() {
//        console.log(`render: width=${this.props.width}, height=${this.props.height}`);
        return (
          <svg width={this.props.width} height={this.props.height}>
            <g ref='graph' />
          </svg>
        );
    }
}