import { Component } from "react";

export class BaseComponent extends Component {
  constructor(props) {
    super(props);

    this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    this.addState = this.addState.bind(this);

    this.state = this.addState({ width: 0, height: 0 });
  }
  addState(data) {
    const state = !!this.state ? this.state : data;
    Object.keys(data).forEach(key => {
      console.log(`setting state.${key} = ${data[key]}`);
      state[key] = data[key];
    });
    return state;
  }
  componentDidMount() {
    this.updateWindowDimensions();
    window.addEventListener("resize", this.updateWindowDimensions);
  }
  componentWillUnmount() {
    window.removeEventListener("resize", this.updateWindowDimensions);
  }
  updateWindowDimensions() {
    const state = this.state;
    state.width = window.innerWidth * 0.7;
    state.height = window.innerHeight * 0.7;
    this.setState(state);
    //    console.log(`state.width = ${state.width}, state.height = ${state.height}`);
  }
}
