#!/bin/sh
#
# Motivation: Post non-java resources to nexus
curl -v \
    -F "r=releases" \
    -F "g=com.joeandlane.resources" \
    -F "a=aspnet" \
    -F "v=2.1" \
    -F "p=tar.gz" \
    -F "file=@./widget-0.1-1.tar.gz" \
    -u myuser:mypassword \
    http://localhost:8081/nexus/service/local/artifact/maven/content
