using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using log4net.Config;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace DnrCore.Controllers
{
  [Route("api/[controller]")]
  public class SampleDataController : Controller
  {
    private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(SampleDataController));
    private static string[] Summaries = new[]
    {
      "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };
    [HttpGet("[action]")]
    public IEnumerable<WeatherForecast> WeatherForecasts()
    {
      var rng = new Random();
      IEnumerable<WeatherForecast> data = Enumerable.Range(1, 5).Select(index => new WeatherForecast
      {
        DateFormatted = DateTime.Now.AddDays(index).ToString("d"),
        TemperatureC = rng.Next(-20, 55),
        Summary = Summaries[rng.Next(Summaries.Length)]
      });
      Log.DebugFormat("WeatherForecasts() {0}", JsonConvert.SerializeObject(data));
      return data;
    }

    public class WeatherForecast
    {
      public string DateFormatted { get; set; }
      public int TemperatureC { get; set; }
      public string Summary { get; set; }

      public int TemperatureF
      {
        get
        {
          return 32 + (int)(TemperatureC / 0.5556);
        }
      }
    }

    [HttpGet("[action]")]
    public IEnumerable<ChartData> GetChartData()
    {
      var rng = new Random();
      IEnumerable<ChartData> data = Enumerable.Range(0, 26).Select(index => new ChartData
      {
        label = index,
        y = rng.Next(0, 75),
        color = null
      });
      // TODO: https://github.com/rpokrovskij/ControlledSerializationJsonConverter
      Log.DebugFormat("GetChartData() {0}", JsonConvert.SerializeObject(data));
      return data;
    }
    public class ChartData
    {
      public int label { get; set; }
      public double y { get; set; }
      public string color { get; set; }
      public string x 
      {
        get 
        {
          return ((char)(label + 65)).ToString();
        }
      }
    }
  // End
    [HttpGet("[action]")]
    public IEnumerable<PieData> GetPieData() 
    {
      var rng = new Random();
      IEnumerable<PieData> data = Enumerable.Range(0, 26).Select(index => new PieData
      {
        index = index,
        value = rng.Next(0, 175),
        color = null
      });
      Log.DebugFormat("GetPieData() {0}", JsonConvert.SerializeObject(data));
      return data;
    }
    public class PieData
    {
      public int index { get; set; }
      public double value { get; set; }
      public string color { get; set; }
      public string key
      {
        get
        {
          return ((char)(index + 65)).ToString();
        }
      }
    }
    public IEnumerable<LineData[]> GetLineDataArrays()
    {
      IEnumerable<LineData[]> glob = null;
      return glob;
    }
    // Generate Line Charts - this is an array of arrays
    [HttpGet("[action]")]
    public IEnumerable<LineData>[] GetLineData()
    {
      var rng = new Random();
      var arrays = rng.Next(2, 20);
      var lines = rng.Next(2, 20);
      var max = rng.Next(10, 100);
      Log.DebugFormat("GetLineData generating {0} arrays of {1} lines of {2} max value each", arrays, lines, max);
      IEnumerable<LineData>[] datas = new IEnumerable<LineData>[arrays];
      for (int i = 0; i < arrays; i++)
      {
        IEnumerable<LineData> data = Enumerable.Range(1, lines).Select(index => new LineData
        {
          x = index,
          y = rng.Next(0, max)
        });
        datas[i] = data;
      }
      Log.DebugFormat("GetLineData() {0}", JsonConvert.SerializeObject(datas));
      return datas;
    }
    public class LineData
    {
      public int x { get; set; }
      public int y { get; set; }
    }
    // Generate Scatter Charts
    [HttpGet("[action]")]
    public IEnumerable<ScatterData> GetScatterData()
    {
      var rng = new Random();
      var lines = rng.Next(2, 20);
      var max = rng.Next(10, 100);
      Log.DebugFormat("GetScatterData generating 1 array of {0} lines of {1} max value each", lines, max);
      IEnumerable<ScatterData> data = Enumerable.Range(1, lines).Select(index => new ScatterData
      {
        type = "Type " + index,
        x = index,
        y = rng.Next(0, max)
      });
      Log.DebugFormat("GetScatterData() {0}", JsonConvert.SerializeObject(data));
      return data;
    }
    public class ScatterData
    {
      public string type { get; set; }
      public int x { get; set; }
      public int y { get; set; }
    }
  }
}
