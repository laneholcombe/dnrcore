﻿// /**
//  * @author: Lane Holcombe
//  * @license: BSD
//  *
//  * MiddlewareExtension.cs
//  * 11/13/2018
//  */
//
using System;
using System.Diagnostics.Contracts;
using System.IO;
using System.Reflection;
using System.ServiceModel.Channels;
using CustomMiddleware;
using log4net.Config;

namespace Microsoft.AspNetCore.Builder
{
  public static class SOAPEndpointExtensions
  {
    private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(SOAPEndpointExtensions));
    // Extension method used to add the middleware to the HTTP request pipeline.
    public static IApplicationBuilder UseSOAPEndpoint<T>(this IApplicationBuilder builder, string path, MessageEncoder encoder)
    {
//      var dnrcore = Assembly.GetEntryAssembly();
//      log4net.Repository.ILoggerRepository logRepository = log4net.LogManager.GetRepository(dnrcore);
//      XmlConfigurator.Configure(logRepository, new FileInfo("log4net.xml"));
//      Log.DebugFormat("UseSOAPEndpoint.  DnrCore is {0}", dnrcore);

      Log.DebugFormat("UseSOAPEndpoint builder: {0},\n\tpath: {1}\n\tencoder: {2}", builder, path, encoder);
      System.Console.WriteLine("\n\n* * *\n* * * UseSOAPEndpoint\n* * *\n");
      if (Contract.Result<IApplicationBuilder>() == null) {
        System.Console.WriteLine("FAIL");
        Log.Warn("FAIL");
      }
      Contract.Ensures(Contract.Result<IApplicationBuilder>() != null);
      System.Console.WriteLine("Contract.Ensures !");
//      return builder.UseMiddleware<SOAPEndpointMiddleware>();
      return builder.UseMiddleware<SOAPEndpointMiddleware>(typeof(T), path, encoder);
    }
    public static IApplicationBuilder UseSOAPEndpoint<T>(this IApplicationBuilder builder, string path, Binding binding)
    {
      Log.DebugFormat("UseSOAPEndpoint path: {0}, binding: {1}", path, binding);
      var encoder = binding.CreateBindingElements().Find<MessageEncodingBindingElement>()?.CreateMessageEncoderFactory().Encoder;
      Log.DebugFormat("encoder is {0},\n\tbuilder is {1},\n\tfor type {2}", encoder, builder, typeof(T));
      IApplicationBuilder value = builder.UseMiddleware<SOAPEndpointMiddleware>(typeof(T), path, encoder);
      return value;
    }
  }
}
