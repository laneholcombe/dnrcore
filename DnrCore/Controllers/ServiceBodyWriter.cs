﻿// /**
//  * @author: Lane Holcombe
//  * @license: BSD
//  *
//  * ServiceBodyWriter.cs
//  * 11/14/2018
//  */
//
using System;
using System.Runtime.Serialization;
using System.ServiceModel.Channels;
using System.Xml;

namespace DnrCore.Controllers
{
  public class ServiceBodyWriter : BodyWriter
  {
    string ServiceNamespace;
    string EnvelopeName;
    string ResultName;
    object Result;

    public ServiceBodyWriter(string serviceNamespace, string envelopeName, string resultName, object result) : base(isBuffered: true)
    {
      ServiceNamespace = serviceNamespace;
      EnvelopeName = envelopeName;
      ResultName = resultName;
      Result = result;
    }

    protected override void OnWriteBodyContents(XmlDictionaryWriter writer)
    {
      writer.WriteStartElement(EnvelopeName, ServiceNamespace);
      var serializer = new DataContractSerializer(Result.GetType(), ResultName, ServiceNamespace);
      serializer.WriteObject(writer, Result);
      writer.WriteEndElement();
    }
  }
}
