﻿/**
 * @author: Lane Holcombe
 * @license: BSD
 *
 * SOAPEndpointMiddleware.cs
 * 11/13/2018
 *
 * From: https://blogs.msdn.microsoft.com/dotnet/2016/09/19/custom-asp-net-core-middleware-example/
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using DnrCore.Controllers;
using Microsoft.AspNetCore.Http;
using CalculatorNameSpace;
using System.Diagnostics.Contracts;

namespace CustomMiddleware
{

  public class SOAPEndpointMiddleware
  {
    private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(SOAPEndpointMiddleware));

    private readonly RequestDelegate _next;
    private readonly Type _serviceType;
    private readonly string _endpointPath;
    private readonly MessageEncoder _messageEncoder;
    private readonly ServiceDescription _service;

    public SOAPEndpointMiddleware(RequestDelegate next, Type serviceType, string path, MessageEncoder encoder)
    {
      Console.WriteLine("SOAPEndpointMiddleware constructor");
      Log.DebugFormat("Create SOAPEndpointMiddleware with next: {0}, serviceType: {1}, and path: {2}", next, serviceType, path);
      _next = next;
      _endpointPath = path;
      _messageEncoder = encoder;
      _serviceType = serviceType;
      _service = new ServiceDescription(serviceType);
      Log.DebugFormat("_service is {0}", _service);
    }

    public async Task Invoke(HttpContext httpContext, IServiceProvider serviceProvider)
    {
      MemoryStream injectedRequestStream = new MemoryStream();

      // Call the next middleware delegate in the pipeline 
      if (httpContext.Request.Path.Equals(_endpointPath, StringComparison.Ordinal))
      {
        Log.DebugFormat("Calling SOAP service for {0}", _endpointPath);
        Message responseMessage;
        var requestLog = $"REQUEST HttpMethod: {httpContext.Request.Method}, Path: {httpContext.Request.Path}";

        using (var bodyReader = new StreamReader(httpContext.Request.Body))
        {
          var bodyAsText = bodyReader.ReadToEnd();
          if (string.IsNullOrWhiteSpace(bodyAsText) == false)
          {
            requestLog += $", Body : {bodyAsText}";
          }

          var bytesToWrite = Encoding.UTF8.GetBytes(bodyAsText);
          injectedRequestStream.Write(bytesToWrite, 0, bytesToWrite.Length);
          injectedRequestStream.Seek(0, SeekOrigin.Begin);
          httpContext.Request.Body = injectedRequestStream;
          Log.DebugFormat("Incoming message: {0}", bodyAsText);
        }
        var requestMessage = _messageEncoder.ReadMessage(httpContext.Request.Body, 0x10000, httpContext.Request.ContentType);
        var soapAction = httpContext.Request.Headers["SOAPAction"].ToString().Trim('\"');
        Log.DebugFormat("soapAction is {0}", soapAction);
        if (!string.IsNullOrEmpty(soapAction))
        {
          Log.Debug("soapAction NOT null | empty");
          requestMessage.Headers.Action = soapAction;
        }

        Log.DebugFormat ("Getting operation {0]", requestMessage.Headers.Action);
        OperationDescription operation = _service.Operations.Where(o => o.SoapAction.Equals(requestMessage.Headers.Action, StringComparison.Ordinal)).FirstOrDefault();
        if (operation == null)
        {
          Log.Debug("operation is null => exception");
          throw new InvalidOperationException($"No operation found for specified action: {requestMessage.Headers.Action}");
        }
        Log.DebugFormat("operation is {0}", operation);

        // Get service type
        Log.DebugFormat("serviceProvider is {0}", serviceProvider);

        Log.DebugFormat("Trying to get service {0}", _service.ServiceType);
        try
        {
          var serviceInstance = serviceProvider.GetService(_service.ServiceType);
          Log.DebugFormat("serviceInstance is {0}", serviceInstance);
          object[]
                  // Get operation arguments from message
                  arguments = GetRequestArguments(requestMessage, operation);
          Log.DebugFormat("arguments is {0}", arguments);
          object
                  // Invoke Operation method
                  responseObject = operation.DispatchMethod.Invoke(serviceInstance, arguments.ToArray());
          Log.DebugFormat("responseObject is {0}", responseObject);
          // Create response message
          var resultName = operation.DispatchMethod.ReturnParameter.GetCustomAttribute<MessageParameterAttribute>()?.Name ?? operation.Name + "Result";
          Log.DebugFormat("resultName is {0}", resultName);
          var bodyWriter = new ServiceBodyWriter(operation.Contract.Namespace, operation.Name + "Response", resultName, responseObject);
          Log.DebugFormat("bodyWriter is {0}", bodyWriter);
          responseMessage = Message.CreateMessage(_messageEncoder.MessageVersion, operation.ReplyAction, bodyWriter);
          Log.DebugFormat("responseMessage is {0}", responseMessage);
          httpContext.Response.ContentType = httpContext.Request.ContentType; // _messageEncoder.ContentType;
          Log.DebugFormat("contentType = {0}", httpContext.Response.ContentType);
          httpContext.Response.Headers["SOAPAction"] = responseMessage.Headers.Action;
          Log.DebugFormat("SOAPAction = {0}", responseMessage.Headers.Action);
          _messageEncoder.WriteMessage(responseMessage, httpContext.Response.Body);
        } catch(Exception exception) {
          Log.WarnFormat("Caught exception while getting serviceInstance: {0}", exception);
        }
      }
      else
      {
        Log.DebugFormat("Forwarding request for {0}", httpContext.Request.Path);
        await _next(httpContext);
      }
    }

    private object[] GetRequestArguments(Message requestMessage, OperationDescription operation)
    {
      var parameters = operation.DispatchMethod.GetParameters();
      var arguments = new List<object>();

      // Deserialize request wrapper and object
      using (var xmlReader = requestMessage.GetReaderAtBodyContents())
      {
        // Find the element for the operation's data
        xmlReader.ReadStartElement(operation.Name, operation.Contract.Namespace);

        for (int i = 0; i < parameters.Length; i++)
        {
          var parameterName = parameters[i].GetCustomAttribute<MessageParameterAttribute>()?.Name ?? parameters[i].Name;
          xmlReader.MoveToStartElement(parameterName, operation.Contract.Namespace);
          if (xmlReader.IsStartElement(parameterName, operation.Contract.Namespace))
          {
            var serializer = new DataContractSerializer(parameters[i].ParameterType, parameterName, operation.Contract.Namespace);
            arguments.Add(serializer.ReadObject(xmlReader, verifyObjectName: true));
          }
        }
      }
      return arguments.ToArray();
    }
  }
}
