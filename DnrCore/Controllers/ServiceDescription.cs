﻿// /**
//  * @author: Lane Holcombe
//  * @license: BSD
//  *
//  * ServiceDescription.cs
//  * 11/14/2018
//  */
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.ServiceModel;

namespace DnrCore.Controllers
{
public class ServiceDescription
{
    private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(ServiceDescription));
    public Type ServiceType { get; private set; }
    public IEnumerable<ContractDescription> Contracts { get; private set; }
    public IEnumerable<OperationDescription> Operations => Contracts.SelectMany(c => c.Operations);

    public ServiceDescription(Type serviceType)
    {
      Log.DebugFormat("ServiceDescription(serviceType: {0})", serviceType);
      ServiceType = serviceType;

      var contracts = new List<ContractDescription>();

      foreach (var contractType in ServiceType.GetInterfaces())
      {
        Log.DebugFormat("working with contractType {0}", contractType);
        foreach (var serviceContract in contractType.GetTypeInfo().GetCustomAttributes<ServiceContractAttribute>())
        {
          Log.DebugFormat("\tWorking with serviceContract {0}", serviceContract);
          contracts.Add(new ContractDescription(this, contractType, serviceContract));
        }
      }
      Log.DebugFormat("Contracts is {0}", contracts);
      Contracts = contracts;
    }
  }
}
