rm -rf DnrCore.tgz bin obj *.log
if [ -d ClientApp/node_modules ]; then
	mv ClientApp/node_modules ..
fi
tar -czf DnrCore.tgz app* C* DnrCore.csproj Dockerfile .gitignore log4net.xml P* S*
if [ -d ../node_modules ]; then
	mv ../node_modules ./ClientApp/
fi
ls -al DnrCore.tgz

