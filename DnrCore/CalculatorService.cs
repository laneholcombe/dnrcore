﻿// /**
//  * @author: Lane Holcombe
//  * @license: BSD
//  *
//  * TestApp.cs
//  * 11/14/2018
//  * From: https://blogs.msdn.microsoft.com/dotnet/2016/09/19/custom-asp-net-core-middleware-example/
//  */
//  Docker images : https://medium.freecodecamp.org/docker-easy-as-build-run-done-e174cc452599
//
using System;
using System.IO;
using System.Reflection;
using System.ServiceModel;
using log4net.Config;

namespace CalculatorNameSpace
  {
    public class CalculatorService : ICalculatorService
    {
      static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(CalculatorService));
      public CalculatorService() {
        var dnrcore = Assembly.GetEntryAssembly();
        log4net.Repository.ILoggerRepository logRepository = log4net.LogManager.GetRepository(dnrcore);
        XmlConfigurator.Configure(logRepository, new FileInfo("log4net.xml"));
        Log.DebugFormat("CalculatorService().  DnrCore is {0}", dnrcore);
      }
      public double Add(double x, double y)
      {
        Log.Debug("Add ...");
        return x + y;
      }
      public double Divide(double x, double y) => x / y;
      public double Multiply(double x, double y) => x * y;
      public double Subtract(double x, double y) => x - y;
    }

    [ServiceContract(Namespace = "http://joeandlane.com/")]
    // Document it.  
    // [WsdlDocumentation("The ICalculatorService contract performs basic calculation services.")]
    public interface ICalculatorService
    {
      [OperationContract] double Add(double x, double y);
      [OperationContract] double Subtract(double x, double y);
      [OperationContract] double Multiply(double x, double y);
      [OperationContract] double Divide(double x, double y);
    }
}
