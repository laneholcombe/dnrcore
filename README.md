## What is this?

This is a demonstration of several technologies that probably should not share the same space. But they do.

I needed a space to practice development work unrelated to my job. My job doesn't consistently provide the kind of work I enjoy. It is not always challenging.

And then it is challenging, just like that.

Anyway ...

## DOT Net on linux is a real thing

See the [Microsoft web site](https://docs.microsoft.com/en-us/dotnet/core/)

Specifically for linux users [see installation instructions](https://docs.microsoft.com/en-us/dotnet/core/linux-prerequisites?tabs=netcore21)

There is a development environment, [MonoDevelop.com](https://www.monodevelop.com/) but caveat emptor: It can spin up dozens of node modules when you build a react-based application.

## REACT! That's what I was interested in ....

But here's the thing: Assuming [this](https://www.itprotoday.com/software-development/apache-foundation-and-facebook-standoff-over-reactjs-license) is true, why would anyone want to use React?

I dunno. But they do.

Anyway, this project started as an implentation of Middleware as described [here](https://blogs.msdn.microsoft.com/dotnet/2016/09/19/custom-asp-net-core-middleware-example/).

Demonstrates .NET application leveraging WCF for SOAP service: ICalculatorService

Some details about CalculatorService and maybe a [link](https://bitbucket.org/laneholcombe/dnrcore/src/cc626648e8239eaeb59616dda7c668b73a836570/DnrCore/CalculatorService.cs)

### Note: Demo was disabled
