﻿// /**
//  * @author: Lane Holcombe
//  * @license: BSD
//  *
//  * Program.cs
//  * 11/14/2018
//  */
//
using System;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace TestClient
{
  public class Program
  {
    static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(Program));
    public static void Main(string[] args)
    {
//      To enable DTD processing set the DtdProcessing property on XmlReaderSettings to Parse and pass the settings into XmlReader.Create method.

      Console.WriteLine("Starting Program.Main()");
      var thisAssembly = Assembly.GetEntryAssembly();
      // log4net.Repository.ILoggerRepository logRepository = log4net.LogManager.GetRepository(thisAssembly);
      // XmlConfigurator.Configure(logRepository, new FileInfo("log4net.xml"));
      Log.DebugFormat("Program start.  Assembly is {0}", thisAssembly);
      Console.WriteLine("Checking args.length");

      if (args.Length < 1)
      {
        Console.WriteLine("Please provide the remote URL of the calculator service");
        return;
      }
      Console.WriteLine("Generating randomizer");

      // Create random inputs
      Random numGen = new Random();
      double x = numGen.NextDouble() * 20;
      double y = numGen.NextDouble() * 20;

      var serviceAddress = $"{args[0]}/CalculatorService.svc";
      Log.DebugFormat("serviceAddress is {0}", serviceAddress);

      var client = new CalculatorServiceClient(new BasicHttpBinding(), new EndpointAddress(serviceAddress));
      Log.DebugFormat("client is {0}", client);

      Log.Debug("Adding");
      Console.WriteLine($"{x} + {y} == {client.Add(x, y)}");
      Log.Debug("Subtracting");
      Console.WriteLine($"{x} - {y} == {client.Subtract(x, y)}");
      Log.Debug("Multiplying");
      Console.WriteLine($"{x} * {y} == {client.Multiply(x, y)}");
      Log.Debug("Dividing");
      Console.WriteLine($"{x} / {y} == {client.Divide(x, y)}");
    }
  }
  class CalculatorServiceClient : ClientBase<ICalculatorService>
  {
    public CalculatorServiceClient(Binding binding, EndpointAddress remoteAddress) : base(binding, remoteAddress) { }
    public double Add(double x, double y) => Channel.Add(x, y);
    public double Subtract(double x, double y) => Channel.Subtract(x, y);
    public double Multiply(double x, double y) => Channel.Multiply(x, y);
    public double Divide(double x, double y) => Channel.Divide(x, y);
  }

  [ServiceContract]
  public interface ICalculatorService
  {
    [OperationContract]
    double Add(double x, double y);
    [OperationContract]
    double Subtract(double x, double y);
    [OperationContract]
    double Multiply(double x, double y);
    [OperationContract]
    double Divide(double x, double y);
  }
}
